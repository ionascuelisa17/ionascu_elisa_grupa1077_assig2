
const FIRST_NAME = "Elisa";
const LAST_NAME = "Ionascu";
const GRUPA = "1077";

/**
 * Make the implementation here
 */
function initCaching() {

    var cache = {};

    cache.pageAccessCounter = function (page) {
        if (page === undefined) page = 'home';
        page = new String(page).toLowerCase();
        if (cache.hasOwnProperty(page)) {
            this[page]++;
        }
        else {
            Object.defineProperty(cache, page, {
                value: 1,
                writable: true
            });
        }
    }

    cache.getCache = function () {
        return this;
    }

    return cache;
}



module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    initCaching
}

